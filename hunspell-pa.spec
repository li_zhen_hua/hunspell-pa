Name:        hunspell-pa
Summary:     Hunspell dictionaries for Punjabi
Version:     1.0.0
Release:     11
Epoch:       1
Source:      http://hunspell.sourceforge.net/pa-demo.tar.gz
URL:         http://hunspell.sourceforge.net
License:     GPLv2+
BuildArch:   noarch

Requires:    hunspell
Supplements: (hunspell and langpacks-pa)

%description
Hunspell dictionaries for Punjabi.

%prep
%autosetup -c -n pa -p1
iconv -f ISO-8859-1 -t UTF-8 pa/Copyright > pa/Copyright.utf8
mv pa/Copyright.utf8 pa/Copyright

%build

%install
install -Dp pa/pa.dic  $RPM_BUILD_ROOT/%{_datadir}/myspell/pa_IN.dic
cp -p pa/pa.aff $RPM_BUILD_ROOT/%{_datadir}/myspell/pa_IN.aff

%files
%doc pa/{README,COPYING,Copyright}
%{_datadir}/myspell/*

%changelog
* Wed Apr 22 2020 lizhenhua <lizhenhua21@huawei.com> - 1:1.0.0-11
- Package init
